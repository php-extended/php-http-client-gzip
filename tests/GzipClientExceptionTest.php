<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-gzip library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\GzipClientException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * GzipClientExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\GzipClientException
 *
 * @internal
 *
 * @small
 */
class GzipClientExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GzipClientException
	 */
	protected GzipClientException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(GzipClientException::class, $this->_object->__toString());
	}
	
	public function testGetRequest() : void
	{
		$this->assertInstanceOf(RequestInterface::class, $this->_object->getRequest());
	}
	
	public function testGetResponse() : void
	{
		$this->assertInstanceOf(ResponseInterface::class, $this->_object->getResponse());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GzipClientException($this->createMock(RequestInterface::class), $this->createMock(ResponseInterface::class), 'Message');
	}
	
}
