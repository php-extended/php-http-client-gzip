<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-gzip library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\GzipClient;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;

/**
 * GzipClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\GzipClient
 *
 * @internal
 *
 * @small
 */
class GzipClientTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GzipClient
	 */
	protected GzipClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testResponseReceived() : void
	{
		$request = $this->getMockForAbstractClass(RequestInterface::class);
		$request->expects($this->any())
			->method('withHeader')
			->willReturn($request)
		;
		
		$this->assertInstanceOf(ResponseInterface::class, $this->_object->sendRequest($request));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = $this->getMockForAbstractClass(ClientInterface::class);
		$client->expects($this->any())
			->method('sendRequest')
			->willReturn($this->getMockForAbstractClass(ResponseInterface::class))
		;
		
		$this->_object = new GzipClient(
			$client,
			$this->getMockForAbstractClass(StreamFactoryInterface::class),
		);
	}
	
}
