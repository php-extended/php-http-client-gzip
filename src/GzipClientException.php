<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-gzip library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Exception;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * GzipClientException class file.
 * 
 * @author Anastaszor
 */
class GzipClientException extends Exception implements ClientExceptionInterface
{
	
	/**
	 * The request.
	 * 
	 * @var RequestInterface
	 */
	protected RequestInterface $_request;
	
	/**
	 * The response.
	 * 
	 * @var ResponseInterface
	 */
	protected ResponseInterface $_response;
	
	/**
	 * Builds a new GzipClientException with the given request and response.
	 * 
	 * @param RequestInterface $request
	 * @param ResponseInterface $response
	 * @param string $message
	 * @param integer $code
	 * @param ?Throwable $previous
	 */
	public function __construct(RequestInterface $request, ResponseInterface $response, string $message, int $code = -1, ?Throwable $previous = null)
	{
		parent::__construct($message, $code, $previous);
		$this->_request = $request;
		$this->_response = $response;
	}
	
	/**
	 * Gets the request.
	 * 
	 * @return RequestInterface
	 */
	public function getRequest() : RequestInterface
	{
		return $this->_request;
	}
	
	/**
	 * Gets the response.
	 * 
	 * @return ResponseInterface
	 */
	public function getResponse() : ResponseInterface
	{
		return $this->_response;
	}
	
}
