# php-extended/php-http-client-gzip
A psr-18 compliant middleware client that handles gz compression and decompression.

![coverage](https://gitlab.com/php-extended/php-http-client-gzip/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-gzip/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-gzip ^8`


## Basic Usage

This library is to make a man in the middle for http requests and responses
and logs the events when requests passes. It may be used the following way :

```php

/** @var $client Psr\Http\Message\ProcessorInterface */ // psr-18
/** @var $request Psr\Http\Message\RequestInterface */  // psr-7

$client = new GzipClient($client);
$response = $client->sendRequest($request);

/** @var $response Psr\Http\Message\ResponseInterface */

```

This library handles the adding of `Accept-Header` on the requests and the
decoding according to the `Content-Encoding` header on the responses.


This library is also able to unzip received gz-like files. For it to work :

- First the request must contain an `X-Php-Download-File` header pointing
to a valid file path. If a file already exists in this location, it will be
overwritten.
- Second, the http client must support the `X-Php-Download-File` header
to write the downloaded file at that location and make a `StreamInterface`
from that file.

If the response that is given to the `X-Php-Download-File` is a gzip
compatible file, it will be uncompressed to the same directory as the current
file is but with a made-up name. All files into the tarball or equivalent
will be concatenated to this location.

This client will set the `X-Php-Uncompressed-File` header with the
location of the newly uncompressed file, and will replace the existing
`StreamInterface` pointing to the downloaded file with a new one pointing
to the uncompressed file.

The downloaded file will not be deleted, it is the job of the calling library
to delete both the downloaded file and the uncompressed file once work has
been done.

```php

$client = new GzipClient($client);
$request = $request->withAddedHeader('X-Php-Download-File' => '/tmp/file.gz');
$response = $client->sendRequest($response);
$stream = $response->getBody();

/** @var $stream \Psr\Http\Message\StreamInterface */
// the stream points to the /tmp/{$sha1} file that is unzipped
// the exact location of the file can be uncovered with :
$filePath = $response->getHeader('X-Php-Uncompressed-File');

```


## License

MIT (See [license file](LICENSE)).
